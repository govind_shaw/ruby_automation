Given (/^I am on Discovery$/)do
	@browser.navigate.to "https://www.discovery.com/"
	sleep(1)
end

When (/^I make favorite videos$/) do
	@selected_videos = Array.new
	element = @browser.find_elements("class","layoutSection__main")[4]
	all_videos = element.find_elements("class", "carousel__tileWrapper")
	all_videos.each_with_index do |e, i|
		favorite_video = e.find_element("class", "icon-plus")
		favorite_video.click
		title_element = e.find_element("class", "showTileSquare__title")
		title = @browser.execute_script("return arguments[0].textContent", title_element)
		@selected_videos << title
		break if i==1
	end
end

And (/^I click on my videos link$/) do
	# @browser.find_element("class","icon-menu").click
	# sleep(1)
	# @browser.find_element("xpath","//*[@id='react-root']/div/div[2]/div[1]/header/nav/div[2]/div/div/div[1]/ul/li[4]/a/span").click
	@browser.navigate.to "https://www.discovery.com/my-videos"
	sleep(3)
	@videos_hash = {}
	favorite_show = @browser.find_element("class", "FavoriteShowsCarousel")
	videos_element = favorite_show.find_elements("class", "showTileSquare__contentBox")
	videos_element.each do |e|
		sleep(1)
		title_element = e.find_element("class", "showTileSquare__title")
		title = @browser.execute_script("return arguments[0].textContent", title_element)
		discription_element = e.find_element("class", "showTileSquare__description")
		discription = @browser.execute_script("return arguments[0].textContent", discription_element)
		@videos_hash[title] = discription
	end
end

When (/^I validate videos$/) do
	raise "No videos matched" if !(@selected_videos-@videos_hash.keys).empty?
end

Then (/^Show title and discription$/) do
	@videos_hash.each_pair do |title, discription|
		puts "title: "+title
		puts "Discription: "+discription
		sleep(1)
	end
end
