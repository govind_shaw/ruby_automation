require 'cucumber'
require 'selenium/webdriver'
require 'pry'

browser = driver = Selenium::WebDriver.for :chrome

Before do |scenario|
	@browser = browser
end

at_exit do
	browser.quit
end