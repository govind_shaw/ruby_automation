#README
 	#Ubantu Setup

		Chrome Driver Setup
		
		1. Go to https://sites.google.com/a/chromium.org/chromedriver/downloads
		2. Download latest chromedriver
		3. Move chromedriver to /usr/local/bin from download directory => mv chromedriver /usr/local/bin
		4. Change owner => chown root:root /usr/local/bin/chromedriver
		5. Directory permison => chmod +x /usr/local/bin/chromedriver
		6. Update chrome browserruby

		Application Setup
		7. git clone https://govind_shaw@bitbucket.org/govind_shaw/ruby_automation.git
		8. Use ruby version >= 2.3.0 or uncomment ruby '2.3.1' in Gemfile => rvm use 2.3.1
		9. Bundle install => bundle install
		10. Go to app path in the terminal => cd download/ruby_automation/
		11. Run cucumber => cucumber